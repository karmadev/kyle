package com.github.karmadeb.kyle.exception;

/**
 * Exception wrapper for when a yaml
 * file gets dumped
 */
public class YamlDumpException extends RuntimeException {

    /**
     * Create the exception
     *
     * @param exception the exception which caused
     *                  the YamlDumpException
     */
    public YamlDumpException(final Throwable exception) {
        super(exception);
    }
}
