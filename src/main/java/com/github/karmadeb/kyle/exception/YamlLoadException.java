package com.github.karmadeb.kyle.exception;

/**
 * Exception wrapper for when a yaml
 * file gets loaded
 */
public class YamlLoadException extends RuntimeException {

    /**
     * Create the exception
     *
     * @param exception the exception which caused
     *                  the YamlLoadException
     */
    public YamlLoadException(final Throwable exception) {
        super(exception);
    }
}
