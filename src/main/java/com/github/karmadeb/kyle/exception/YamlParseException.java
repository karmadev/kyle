package com.github.karmadeb.kyle.exception;

/**
 * Exception wrapper for when a yaml
 * file gets parsed
 */
public class YamlParseException extends RuntimeException {

    /**
     * Create the exception
     *
     * @param exception the exception which caused
     *                  the YamlParseException
     */
    public YamlParseException(final Throwable exception) {
        super(exception);
    }
}
