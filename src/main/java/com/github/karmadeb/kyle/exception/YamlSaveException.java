package com.github.karmadeb.kyle.exception;

/**
 * Exception wrapper for when a yaml
 * file gets saved
 */
public class YamlSaveException extends RuntimeException {

    /**
     * Create the exception
     *
     * @param exception the exception which caused
     *                  the YamlSaveException
     */
    public YamlSaveException(final Throwable exception) {
        super(exception);
    }
}
