package com.github.karmadeb.kyle;

import java.nio.file.Path;
import java.util.Map;

/**
 * Yaml section
 */
class YamlSection extends SimpleYamlContent {

    /**
     * Initialize the yaml handler
     *
     * @param data the yaml data
     */
    YamlSection(final Map<String, Object> data) {
        super(data);
    }

    /**
     * Save the current yaml file handler
     * to the specified file
     *
     * @param path the yaml file path
     * @return the saved yaml file
     */
    @Override
    public YamlSection saveTo(final Path path) {
        return this;
    }

    /**
     * Save the yaml file
     */
    @Override
    public void save() {}
}

