package com.github.karmadeb.kyle;

import com.github.karmadeb.kyle.exception.YamlDumpException;
import com.github.karmadeb.kyle.exception.YamlLoadException;
import com.github.karmadeb.kyle.exception.YamlParseException;
import com.github.karmadeb.kyle.exception.YamlSaveException;
import com.github.karmadeb.kyle.reader.YamlReader;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Represents a yaml content. The content
 * can be built from a map, a string or even
 * a file
 */
@SuppressWarnings("unused")
public interface YamlContent {

    /**
     * Create an in-memory yaml
     * content
     *
     * @return the yaml content
     */
    static YamlContent empty() {
        return new SimpleYamlContent();
    }

    /**
     * Create (or load) a yaml content
     * from the specified file
     *
     * @param file the file to create and/or
     *             read from
     * @return the yaml content
     * @throws YamlLoadException if the file fails to read or create
     */
    static YamlContent load(final Path file) throws YamlLoadException {
        return load(file, null);
    }

    /**
     * Loads a yaml content from the
     * specified resource
     *
     * @param stream the resource stream
     * @return the yaml content
     * @throws YamlLoadException if the stream fails to read
     */
    static YamlContent load(final InputStream stream) throws YamlLoadException {
        YamlReader reader = new YamlReader(stream);
        return reader.getContent();
    }

    /**
     * Loads a yaml content from the
     * specified file using the provided
     * schema
     *
     * @param file the file to create and/or
     *             read from
     * @param schema the yaml schema
     * @return the yaml content
     * @throws YamlLoadException if the file fails to read or create, or
     * the schema fails to read or create
     */
    static YamlContent load(final Path file, final InputStream schema) throws YamlLoadException {
        if (!Files.exists(file)) {
            try {
                Path parent = file.toAbsolutePath().getParent();
                if (!Files.exists(parent))
                    Files.createDirectories(parent);

                Files.createFile(file);
            } catch (IOException ex) {
                throw new YamlLoadException(ex);
            }
        }

        YamlReader reader = (schema != null ? new YamlReader(schema) : null);
        try {
            byte[] rawContents = Files.readAllBytes(file);
            String content = new String(rawContents);

            Yaml yaml = new Yaml();
            Map<String, Object> data = yaml.load(content);

            return new SimpleYamlContent(file, data, reader);
        } catch (IOException ex) {
            throw new YamlLoadException(ex);
        }
    }

    /**
     * Parse the data
     *
     * @param data the data to parse
     * @return the parsed content
     */
    static YamlContent parse(final byte[] data) {
        String yamlString = new String(data);
        return parse(yamlString);
    }

    /**
     * Parse the yaml string
     *
     * @param yamlString the yaml string
     *                   to parse
     * @return the parsed content
     */
    static YamlContent parse(final String yamlString) {
        Yaml yaml = new Yaml();
        Map<String, Object> data = yaml.load(yamlString);

        return parse(data);
    }

    /**
     * Parse the raw yaml data
     *
     * @param rawData the raw data
     * @return the parse content
     */
    static YamlContent parse(final Map<String, Object> rawData) {
        return new SimpleYamlContent(rawData);
    }

    /**
     * Parse the data
     *
     * @param data the data to parse
     * @param schema the data schema
     * @return the parsed content
     * @throws YamlParseException if the schema fails to parse
     */
    static YamlContent parse(final byte[] data, final InputStream schema) throws YamlParseException {
        String yamlString = new String(data);
        return parse(yamlString, schema);
    }

    /**
     * Parse the yaml string
     *
     * @param yamlString the yaml string
     *                   to parse
     * @param schema the data schema
     * @return the parsed content
     * @throws YamlParseException if the schema fails to parse
     */
    static YamlContent parse(final String yamlString, final InputStream schema) throws YamlParseException {
        Yaml yaml = new Yaml();
        Map<String, Object> data = yaml.load(yamlString);

        return parse(data, schema);
    }

    /**
     * Parse the raw yaml data
     *
     * @param rawData the raw data
     * @param schema the data schema
     * @return the parse content
     * @throws YamlParseException if the schema fails to parse
     */
    static YamlContent parse(final Map<String, Object> rawData, final InputStream schema) throws YamlParseException {
        YamlReader reader = new YamlReader(schema);
        return new SimpleYamlContent(null, rawData, reader);
    }

    /**
     * Get the yaml file raw data
     *
     * @return the yaml file raw data
     */
    Map<String, Object> rawData();

    /**
     * Import data from the other file handler
     *
     * @param other the other file handler
     * @param replaceExisting replace the current data with
     *                        the other yaml data
     */
    void importFrom(final YamlContent other, final boolean replaceExisting);

    /**
     * Validate the current file
     * @throws YamlSaveException if the file fails to save
     * @throws YamlDumpException if the file fails to dump
     */
    void validate();

    /**
     * Reload the current file handle
     */
    void reload();

    /**
     * Compare the objects
     *
     * @param path the path to the value
     * @param value the value to compare
     * @return if the values are the same type
     */
    @SuppressWarnings("all")
    boolean compareValue(final String path, final Object value);

    /**
     * Get an object
     *
     * @param path the object path
     * @param def the object default value
     * @return the value or default value if
     * not found
     */
    Object get(final String path, final Object def);

    /**
     * Get an object
     *
     * @param path the object path
     * @return the value
     */
    default Object get(final String path) {
        return get(path, null);
    }

    /**
     * Get an enum constant from a value type
     *
     * @param path the path to the enum value
     * @param defaultValue the enum default value if no
     *                     value is present or if the value does not
     *                     reference an existing enum constant
     * @param enumType the enum type
     * @return the enum constant value
     * @param <T> the enum object type
     */
    <T extends Enum<?>> T getEnum(final String path, final T defaultValue, final Class<? extends T> enumType);

    /**
     * Get an enum constant from a value type
     *
     * @param path the path to the enum value
     * @param enumType the enum type
     * @return the enum constant value
     * @param <T> the enum object type
     */
    default <T extends Enum<T>> T getEnum(final String path, final Class<? extends T> enumType) {
        return this.getEnum(path, null, enumType);
    }

    /**
     * Get a string
     *
     * @param path the string path
     * @param def the string default value
     * @return the value or default value if
     * not found
     */
    String getString(final String path, final String def);

    /**
     * Get a string
     *
     * @param path the string path
     * @return the value
     */
    default String getString(final String path) {
        return getString(path, null);
    }

    /**
     * Get a character
     *
     * @param path the character path
     * @param def the character default value
     * @return the value or default value if
     * not found
     */
    char getCharacter(final String path, final char def);

    /**
     * Get a character
     *
     * @param path the character path
     * @return the value
     */
    default char getCharacter(final String path) {
        return getCharacter(path, '\0');
    }

    /**
     * Get a byte
     *
     * @param path the byte path
     * @param def the byte default value
     * @return the value or default value if
     * not found
     */
    byte getByte(final String path, final byte def);

    /**
     * Get a byte
     *
     * @param path the byte path
     * @return the value
     */
    default byte getByte(final String path) {
        return getByte(path, (byte) 0x00000000);
    }

    /**
     * Get a short
     *
     * @param path the short path
     * @param def the short default value
     * @return the value or default value if
     * not found
     */
    short getShort(final String path, final short def);

    /**
     * Get a short
     *
     * @param path the short path
     * @return the value
     */
    default short getShort(final String path) {
        return getShort(path, (byte) 0x00000000);
    }

    /**
     * Get an integer
     *
     * @param path the integer path
     * @param def the integer default value
     * @return the value or default value if
     * not found
     */
    int getInteger(final String path, final int def);

    /**
     * Get an integer
     *
     * @param path the integer path
     * @return the value
     */
    default int getInteger(final String path) {
        return getInteger(path, (byte) 0x00000000);
    }

    /**
     * Get a long
     *
     * @param path the long path
     * @param def the long default value
     * @return the value or default value if
     * not found
     */
    long getLong(final String path, final long def);

    /**
     * Get a long
     *
     * @param path the long path
     * @return the value
     */
    default long getLong(final String path) {
        return getLong(path, (byte) 0x00000000);
    }

    /**
     * Get a double
     *
     * @param path the double path
     * @param def the double default value
     * @return the value or default value if
     * not found
     */
    double getDouble(final String path, final double def);

    /**
     * Get a double
     *
     * @param path the double path
     * @return the value
     */
    default double getDouble(final String path) {
        return getDouble(path, (byte) 0x00000000);
    }

    /**
     * Get a float
     *
     * @param path the float path
     * @param def the float default value
     * @return the value or default value if
     * not found
     */
    float getFloat(final String path, final float def);

    /**
     * Get a float
     *
     * @param path the float path
     * @return the value
     */
    default float getFloat(final String path) {
        return getFloat(path, (byte) 0x00000000);
    }

    /**
     * Get a boolean
     *
     * @param path the boolean path
     * @param def the boolean default value
     * @return the value or default value if
     * not found
     */
    boolean getBoolean(final String path, final boolean def);

    /**
     * Get a boolean
     *
     * @param path the boolean path
     * @return the value
     */
    default boolean getBoolean(final String path) {
        return getBoolean(path, false);
    }

    /**
     * Get a serialized instance
     *
     * @param path the instance path
     * @param def the instance default value
     * @return the value or default value if
     * not found
     *
     * @throws IOException if the serialization fails
     * @throws ClassNotFoundException if the serialization loads a class which does not exist anymore
     */
    Object getSerialized(final String path, final Object def) throws IOException, ClassNotFoundException;

    /**
     * Get a serialized instance
     *
     * @param path the instance path
     * @return the value
     *
     * @throws IOException if the serialization fails
     * @throws ClassNotFoundException if the serialization loads a class which does not exist anymore
     */
    default Object getSerialized(final String path) throws IOException, ClassNotFoundException {
        return getSerialized(path, null);
    }

    /**
     * Get a list
     *
     * @param path the list path
     * @param def the default values to add on the
     *            list
     * @return the list
     */
    default List<String> getList(final String path, final String... def) {
        return getList(path, Arrays.asList(def));
    }

    /**
     * Get a list
     *
     * @param path the list path
     * @param ifNull the list if the path does not point
     *               to a list or is not set
     * @return the list
     */
    List<String> getList(final String path, final Iterable<String> ifNull);

    /**
     * Get a section
     *
     * @param path the section path
     * @return the section
     */
    YamlContent getSection(final String path);

    /**
     * Check if the path is set
     *
     * @param path the path
     * @return if the path is set
     */
    boolean isSet(final String path);

    /**
     * Check if none of the paths are set. If the
     * paths are empty, it will return true if the file
     * is empty (has no keys defined)
     *
     * @param paths the paths
     * @return if the paths are set
     */
    default boolean isNoneSet(final String... paths) {
        return isNoneSet(Arrays.asList(paths));
    }

    /**
     * Check if none of the paths are set. If the
     * paths are empty, it will return true if the file
     * is empty (has no keys defined)
     *
     * @param paths the paths
     * @return if the paths are set
     */
    default boolean isNoneSet(final Collection<String> paths) {
        if (paths.isEmpty()) return getKeys(true).isEmpty();

        for (String path : paths) {
            if (isSet(path)) return false;
        }

        return true;
    }

    /**
     * Check if all the paths are set. If the
     * paths are empty, it will return false if the file
     * is empty (has no keys defined)
     *
     * @param paths the paths
     * @return if the paths are set
     */
    default boolean isAllSet(final String... paths) {
        return isNoneSet(Arrays.asList(paths));
    }

    /**
     * Check if all the paths are set. If the
     * paths are empty, it will return false if the file
     * is empty (has no keys defined)
     *
     * @param paths the paths
     * @return if the paths are set
     */
    default boolean isAllSet(final Collection<String> paths) {
        if (paths.isEmpty()) return !getKeys(true).isEmpty();

        for (String path : paths) {
            if (!isSet(path)) return false;
        }

        return true;
    }

    /**
     * Check if any of the paths are not set. If the
     * paths are empty, it will return true if the file
     * is empty (has no keys defined)
     *
     * @param paths the paths
     * @return if any of the paths are not set
     */
    default boolean isAnyMissing(final String... paths) {
        return isNoneSet(Arrays.asList(paths));
    }

    /**
     * Check if any of the paths are not set. If the
     * paths are empty, it will return true if the file
     * is empty (has no keys defined)
     *
     * @param paths the paths
     * @return if any of the paths are not set
     */
    default boolean isAnyMissing(final Collection<String> paths) {
        if (paths.isEmpty()) return getKeys(true).isEmpty();

        for (String path : paths) {
            if (!isSet(path)) return true;
        }

        return false;
    }

    /**
     * Check if any the paths are set. If the
     * paths are empty, it will return false if the file
     * is empty (has no keys defined)
     *
     * @param paths the paths
     * @return if any of the paths are set
     */
    default boolean isAnySet(final String... paths) {
        return isNoneSet(Arrays.asList(paths));
    }

    /**
     * Check if any the paths are set. If the
     * paths are empty, it will return false if the file
     * is empty (has no keys defined)
     *
     * @param paths the paths
     * @return if any of the paths are set
     */
    default boolean isAnySet(final Collection<String> paths) {
        if (paths.isEmpty()) return !getKeys(true).isEmpty();

        for (String path : paths) {
            if (isSet(path)) return true;
        }

        return false;
    }

    /**
     * Returns the first path which is set. If the paths
     * are empty, it will return always null. If no path is
     * found, it will also return null
     *
     * @param paths the paths
     * @return the first set path
     */
    default String getFirstAvailable(final String... paths) {
        return getFirstAvailable(Arrays.asList(paths));
    }

    /**
     * Returns the first path which is set. If the paths
     * are empty, it will return always null. If no path is
     * found, it will also return null
     *
     * @param paths the paths
     * @return the first set path
     */
    default String getFirstAvailable(final Collection<String> paths) {
        if (paths.isEmpty()) return null;

        for (String path : paths) {
            if (isSet(path)) {
                return path;
            }
        }

        return null;
    }

    /**
     * Returns the first path which is not set. If the paths
     * are empty, it will return always null. If all paths are
     * found, it will also return null
     *
     * @param paths the paths
     * @return the first missing path
     */
    default String getFirstMissing(final String... paths) {
        return getFirstAvailable(Arrays.asList(paths));
    }

    /**
     * Returns the first path which is not set. If the paths
     * are empty, it will return always null. If all paths are
     * found, it will also return null
     *
     * @param paths the paths
     * @return the first missing path
     */
    default String getFirstMissing(final Collection<String> paths) {
        if (paths.isEmpty()) return null;

        for (String path : paths) {
            if (!isSet(path)) {
                return path;
            }
        }

        return null;
    }

    /**
     * Check if the path is a section
     *
     * @param path the path
     * @return if the path is a section
     */
    boolean isSection(final String path);

    /**
     * Get all the keys
     *
     * @param deep get keys and sub keys
     * @return the keys
     */
    Collection<String> getKeys(final boolean deep);

    /**
     * Save data
     *
     * @param path the data path
     * @param value the data value
     */
    void set(final String path, final String value);

    /**
     * Save data
     *
     * @param path the data path
     * @param value the data value
     */
    void set(final String path, final char value);

    /**
     * Save data
     *
     * @param path the data path
     * @param value the data value
     */
    void set(final String path, final byte value);

    /**
     * Save data
     *
     * @param path the data path
     * @param value the data value
     */
    void set(final String path, final short value);

    /**
     * Save data
     *
     * @param path the data path
     * @param value the data value
     */
    void set(final String path, final int value);

    /**
     * Save data
     *
     * @param path the data path
     * @param value the data value
     */
    void set(final String path, final long value);

    /**
     * Save data
     *
     * @param path the data path
     * @param value the data value
     */
    void set(final String path, final double value);

    /**
     * Save data
     *
     * @param path the data path
     * @param value the data value
     */
    void set(final String path, final float value);

    /**
     * Save data
     *
     * @param path the data path
     * @param value the data value
     */
    void set(final String path, final boolean value);

    /**
     * Save data
     *
     * @param path the data path
     * @param value the data value
     */
    void set(final String path, final Serializable value);

    /**
     * Save data
     *
     * @param path the data path
     * @param values the data value
     */
    default void set(final String path, final String[] values) {
        this.set(path, Arrays.asList(values));
    }

    /**
     * Save data
     *
     * @param path the data path
     * @param values the data value
     */
    void set(final String path, final Iterable<String> values);

    /**
     * Save data
     *
     * @param path the data path
     * @param section the data value
     */
    void set(final String path, final YamlContent section);

    /**
     * Get the yaml file raw data
     *
     * @return the yaml raw data
     */
    Map<String, Object> raw();

    /**
     * Get the yaml handle
     *
     * @return the yaml handle
     */
    Path handle();

    /**
     * Save the current yaml file handler
     * to the specified file
     *
     * @param path the yaml file path
     * @return the saved yaml file
     * @throws YamlSaveException if the file fails to save
     * @throws YamlDumpException if the file fails to dump
     */
    YamlContent saveTo(final Path path) throws YamlSaveException, YamlDumpException;

    /**
     * Save the yaml file
     *
     * @throws YamlSaveException if the file fails to save
     * @throws YamlDumpException if the file fails to dump
     */
    void save() throws YamlSaveException, YamlDumpException;

    /**
     * Get the string representation of
     * this yaml file
     *
     * @return the string representation of
     * this yaml file
     * @throws YamlDumpException if the file fails to dump
     */
    String toString() throws YamlDumpException;
}
