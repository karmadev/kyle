package com.github.karmadeb.kyle;

import com.github.karmadeb.kyle.exception.YamlDumpException;
import com.github.karmadeb.kyle.exception.YamlSaveException;
import com.github.karmadeb.kyle.reader.YamlDumper;
import com.github.karmadeb.kyle.reader.YamlReader;
import com.github.karmadeb.kyle.reader.YamlType;
import com.github.karmadeb.kyle.util.EnumUtils;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@SuppressWarnings("unused")
class SimpleYamlContent implements YamlContent {

    /**
     * Yaml file
     */
    private final Path file;
    /**
     * Yaml data
     */
    private final Map<String, Object> data = new LinkedHashMap<>();
    /**
     * Yaml source
     */
    private final YamlReader source;

    SimpleYamlContent() {
        this(null, new LinkedHashMap<>(), null);
    }

    /**
     * Initialize the yaml handler
     *
     * @param data the yaml data
     */
    SimpleYamlContent(final Map<String, Object> data) {
        this(null, data, null);
    }

    /**
     * Initialize the yaml handler
     *
     * @param file the yaml file
     * @param data the yaml data
     */
    SimpleYamlContent(final Path file, final Map<String, Object> data) {
        this(file, data, null);
    }

    /**
     * Initialize the yaml handler
     *
     * @param file the yaml file
     * @param data the yaml data
     * @param source the yaml source
     */
    SimpleYamlContent(final Path file, final Map<String, Object> data, final YamlReader source) {
        this.file = file;
        if (data != null)
            this.data.putAll(data);

        this.source = source;
    }

    /**
     * Get the yaml file raw data
     *
     * @return the yaml file raw data
     */
    @Override
    public Map<String, Object> rawData() {
        return new HashMap<>(data);
    }

    /**
     * Import data from the other file handler
     *
     * @param other           the other file handler
     * @param replaceExisting replace the current data with
     *                        the other yaml data
     */
    @Override
    public void importFrom(final YamlContent other, final boolean replaceExisting) {
        Map<String, Object> data = other.rawData();
        if (replaceExisting) {
            this.data.putAll(data);
        } else {
            for (String key : other.getKeys(true)) {
                if (this.isSet(key)) {
                    this.save(key, other.get(key));
                }
            }
        }
    }

    /**
     * Validate the current file
     * @throws YamlSaveException if the file fails to save
     * @throws YamlDumpException if the file fails to dump
     */
    @Override
    public void validate() throws YamlSaveException, YamlDumpException {
        if (source == null) {
            return;
        }

        boolean modifications = false;

        YamlDumper dumper = source.getDumper();
        YamlContent handler = source.getContent();
        for (String key : handler.getKeys(true)) {
            Object handleValue = handler.get(key);

            if (!isSet(key)) {
                save(key, handleValue);
                modifications = true;
            } else {
                if (dumper != null && !dumper.validateType(key, this.get(key))) {
                    save(key, handleValue);
                    modifications = true;
                } else if (!compareValue(key, handleValue)) {
                    save(key, handleValue);
                    modifications = true;
                }
            }
        }

        if (modifications) save();
    }

    private static Object asSchemeValue(Object value) {
        if (value instanceof String) {
            value = "";
        } else if (value instanceof Number) {
            value = 0;
        } else if (value instanceof Iterable) {
            value = Collections.emptyList();
        } else if (value instanceof Boolean) {
            value = false;
        }
        return value;
    }

    /**
     * Reload the current file handle
     */
    @Override
    public void reload() {
        if (file == null) return; //We cannot reload a virtual yaml file
        SimpleYamlContent sym = (SimpleYamlContent) YamlContent.load(file);

        data.clear();
        data.putAll(sym.data);
    }

    /**
     * Compare the objects
     *
     * @param path  the path to the value
     * @param value the value to compare
     * @return if the values are the same type
     */
    @Override
    public boolean compareValue(final String path, final Object value) {
        Object current = get(path);
        if (current == null) return value == null;

        if (current instanceof CharSequence) {
            String sequence = ((CharSequence) current).toString();

            if (value instanceof Character) return sequence.length() == 1;
            if (value instanceof Boolean) {
                return sequence.equalsIgnoreCase("y") ||
                        sequence.equalsIgnoreCase("1") ||
                        sequence.equalsIgnoreCase("true") ||
                        sequence.equalsIgnoreCase("n") ||
                        sequence.equalsIgnoreCase("0") ||
                        sequence.equalsIgnoreCase("false");
            }

            return value instanceof CharSequence;
        }
        if (current instanceof Number) {
            if (value instanceof Boolean) {
                Number number = (Number) current;
                return number.shortValue() == 0 || number.shortValue() == 1;
            }
            return value instanceof Number;
        }

        if (current instanceof Boolean) {
            return value instanceof Boolean;
        }
        return current.getClass().isAssignableFrom(value.getClass()) || value.getClass().isAssignableFrom(current.getClass()) ||
                current.getClass().isInstance(value) || value.getClass().isInstance(current);
    }

    /**
     * Get an object
     *
     * @param path the object path
     * @param def  the object default value
     * @return the value or default value if
     * not found
     */
    @Override @SuppressWarnings("unchecked")
    public Object get(final String path, final Object def) {
        if (path.contains(".")) {
            String[] pathData = path.split("\\.");
            Map<String, Object> currentData = data;
            for (int i = 0; i < pathData.length; i++) {
                String dir = pathData[i];
                Object tmpValue = currentData.getOrDefault(dir, null);

                if (i == pathData.length - 1) return tmpValue;
                if (!(tmpValue instanceof Map)) return def;

                currentData = (Map<String, Object>) tmpValue;
            }
        }

        return data.getOrDefault(path, def);
    }

    /**
     * Get an enum constant from a value type
     *
     * @param path         the path to the enum value
     * @param defaultValue the enum default value if no
     *                     value is present or if the value does not
     *                     reference an existing enum constant
     * @param enumType     the enum type
     * @return the enum constant value
     */
    @Override
    public <T extends Enum<?>> T getEnum(final String path, final T defaultValue, final Class<? extends T> enumType) {
        if (this.source == null)
            return defaultValue;

        YamlDumper dumper = this.source.getDumper();
        if (dumper == null)
            return defaultValue;

        if (!dumper.getYamlType(path).equals(YamlType.ENUM))
            return defaultValue;

        String rawValue = this.getString(path);
        String enumValue = EnumUtils.handlePotentialEnumValue(rawValue);
        if (dumper.validateType(path, enumValue)) {
            T[] constants = enumType.getEnumConstants();
            for (T constant : constants) {
                if (constant.name().equalsIgnoreCase(enumValue))
                    return constant;
            }
        }

        return defaultValue;
    }

    /**
     * Get a string
     *
     * @param path the string path
     * @param def  the string default value
     * @return the value or default value if
     * not found
     */
    @Override
    public String getString(final String path, final String def) {
        Object value = get(path, def);
        if (value instanceof List) {
            List<String> list = getList(path);
            return collectionToString(list);
        }

        if (!(value instanceof String)) return def;

        return (String) value;
    }

    /**
     * Get a character
     *
     * @param path the character path
     * @param def  the character default value
     * @return the value or default value if
     * not found
     */
    @Override
    public char getCharacter(final String path, final char def) {
        Object value = get(path, def);
        if (!(value instanceof String)) return def;

        return ((String) value).charAt(0);
    }

    /**
     * Get a byte
     *
     * @param path the byte path
     * @param def  the byte default value
     * @return the value or default value if
     * not found
     */
    @Override
    public byte getByte(final String path, final byte def) {
        Object value = get(path, def);
        if (!(value instanceof Number)) return def;

        return ((Number) value).byteValue();
    }

    /**
     * Get a short
     *
     * @param path the short path
     * @param def  the short default value
     * @return the value or default value if
     * not found
     */
    @Override
    public short getShort(final String path, final short def) {
        Object value = get(path, def);
        if (!(value instanceof Number)) return def;

        return ((Number) value).shortValue();
    }

    /**
     * Get an integer
     *
     * @param path the integer path
     * @param def  the integer default value
     * @return the value or default value if
     * not found
     */
    @Override
    public int getInteger(final String path, final int def) {
        Object value = get(path, def);
        if (!(value instanceof Number)) return def;

        return ((Number) value).intValue();
    }

    /**
     * Get a long
     *
     * @param path the long path
     * @param def  the long default value
     * @return the value or default value if
     * not found
     */
    @Override
    public long getLong(final String path, final long def) {
        Object value = get(path, def);
        if (!(value instanceof Number)) return def;

        return ((Number) value).longValue();
    }

    /**
     * Get a double
     *
     * @param path the double path
     * @param def  the double default value
     * @return the value or default value if
     * not found
     */
    @Override
    public double getDouble(final String path, final double def) {
        Object value = get(path, def);
        if (!(value instanceof Number)) return def;

        return ((Number) value).doubleValue();
    }

    /**
     * Get a float
     *
     * @param path the float path
     * @param def  the float default value
     * @return the value or default value if
     * not found
     */
    @Override
    public float getFloat(final String path, final float def) {
        Object value = get(path, def);
        if (!(value instanceof Number)) return def;

        return ((Number) value).floatValue();
    }

    /**
     * Get a boolean
     *
     * @param path the boolean path
     * @param def  the boolean default value
     * @return the value or default value if
     * not found
     */
    @Override
    public boolean getBoolean(final String path, final boolean def) {
        Object value = get(path, def);
        if (value instanceof Number) {
            Number number = (Number) value;
            short integer = number.shortValue();
            if (integer == 1) return true;
            if (integer == 0) return false;
            return def;
        }
        if (value instanceof String) {
            String string = (String) value;
            if (string.equalsIgnoreCase("y") || string.equalsIgnoreCase("true") || string.equalsIgnoreCase("1")) return true;
            if (string.equalsIgnoreCase("n") || string.equalsIgnoreCase("false") || string.equalsIgnoreCase("2")) return false;
            return def;
        }

        if (!(value instanceof Boolean)) return def;

        return (boolean) value;
    }

    /**
     * Get a serialized instance
     *
     * @param path the instance path
     * @param def  the instance default value
     * @return the value or default value if
     * not found
     */
    @Override
    public Object getSerialized(final String path, final Object def) {
        Object value = get(path, def);
        if (!(value instanceof String)) return def;

        byte[] data = Base64.getDecoder().decode((String) value);
        try(ByteArrayInputStream bi = new ByteArrayInputStream(data); ObjectInputStream si = new ObjectInputStream(bi)) {
            return si.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Get a list
     *
     * @param path   the list path
     * @param ifNull the list if the path does not point
     *               to a list or is not set
     * @return the list
     */
    @Override
    public List<String> getList(final String path, final Iterable<String> ifNull) {
        Object value = get(path, ifNull);
        if (!(value instanceof List))
            return collectIterable(ifNull);

        List<?> unknownList = (List<?>) value;
        List<String> result = new ArrayList<>();
        for (Object object : unknownList) {
            if (object != null) {
                result.add(String.valueOf(object));
            }
        }

        return result;
    }

    /**
     * Get a section
     *
     * @param path the section path
     * @return the section
     */
    @Override
    public @SuppressWarnings("unchecked") YamlContent getSection(final String path) {
        Object value = get(path);
        if (!(value instanceof Map)) return null;

        Map<String, Object> map = (Map<String, Object>) value;
        return new YamlSection(map);
    }

    /**
     * Check if the path is set
     *
     * @param path the path
     * @return if the path is set
     */
    @Override
    public boolean isSet(final String path) {
        return get(path) != null;
    }

    /**
     * Check if the path is a section
     *
     * @param path the path
     * @return if the path is a section
     */
    @Override
    public boolean isSection(final String path) {
        return get(path) instanceof Map;
    }

    /**
     * Get all the keys
     *
     * @param deep get keys and sub keys
     * @return the keys
     */
    @Override @SuppressWarnings("unchecked")
    public Collection<String> getKeys(final boolean deep) {
        List<String> keys = new ArrayList<>(data.keySet());

        if (deep) {
            List<String> deepKeys = new ArrayList<>();

            String currentPath = "";
            for (String key : keys) {
                Object value = data.getOrDefault(key, null);
                if (value == null) continue;

                if (value instanceof Map) {
                    deepKeys.add(key);
                    Map<String, Object> data = (Map<String, Object>) value;
                    deepKeys.addAll(mapKeys(key + ".", data));
                } else {
                    deepKeys.add(currentPath + key);
                }
            }

            return deepKeys;
        }

        return keys;
    }

    /**
     * Map the keys of a map
     *
     * @param path the main path
     * @param data the map data
     * @return the recursive keys
     */
    @SuppressWarnings("unchecked")
    private Collection<String> mapKeys(final String path, final Map<String, Object> data) {
        List<String> deepKeys = new ArrayList<>();

        for (String key : data.keySet()) {
            Object value = data.getOrDefault(key, null);
            if (value == null) continue;

            if (value instanceof Map) {
                deepKeys.add(path + key);
                Map<String, Object> subData = (Map<String, Object>) value;
                deepKeys.addAll(mapKeys(path + key + ".", subData));
            } else {
                deepKeys.add(path + key);
            }
        }

        return deepKeys;
    }

    /**
     * Save a value
     *
     * @param path the path
     * @param value the value
     */
    @SuppressWarnings("unchecked")
    private void save(final String path, final Object value) {
        if (path.contains(".")) {
            String[] pathData = path.split("\\.");
            Map<String, Object> node = data;

            for (int i = 0; i < pathData.length; i++) {
                String dir = pathData[i];
                Object dataValue = node.getOrDefault(dir, null);
                if (dataValue == null) {
                    if (i == pathData.length - 1) {
                        node.put(dir, value);
                    } else {
                        Map<String, Object> map = new LinkedHashMap<>();
                        node.put(dir, map);
                        node = map;
                    }
                } else {
                    if (i == pathData.length - 1) {
                        if (!(dataValue instanceof Map)) {
                            node.put(dir, value);
                        } else {
                            if (value instanceof Map) {
                                node.put(dir, value);
                            }
                        }
                    } else {
                        Object nodeValue = node.getOrDefault(dir, null);
                        if (nodeValue == null) nodeValue = new LinkedHashMap<>();
                        if (!(nodeValue instanceof Map)) return;

                        node = (Map<String, Object>) nodeValue;
                    }
                }
            }
        } else {
            data.put(path, value);
        }
    }

    /**
     * Save data
     *
     * @param path  the data path
     * @param value the data value
     */
    @Override
    public void set(final String path, final String value) {
        save(path, value);
    }

    /**
     * Save data
     *
     * @param path  the data path
     * @param value the data value
     */
    @Override
    public void set(final String path, final char value) {
        save(path, String.valueOf(value));
    }

    /**
     * Save data
     *
     * @param path  the data path
     * @param value the data value
     */
    @Override
    public void set(final String path, final byte value) {
        save(path, value);
    }

    /**
     * Save data
     *
     * @param path  the data path
     * @param value the data value
     */
    @Override
    public void set(final String path, final short value) {
        save(path, value);
    }

    /**
     * Save data
     *
     * @param path  the data path
     * @param value the data value
     */
    @Override
    public void set(final String path, final int value) {
        save(path, value);
    }

    /**
     * Save data
     *
     * @param path  the data path
     * @param value the data value
     */
    @Override
    public void set(final String path, final long value) {
        save(path, value);
    }

    /**
     * Save data
     *
     * @param path  the data path
     * @param value the data value
     */
    @Override
    public void set(final String path, final double value) {
        save(path, value);
    }

    /**
     * Save data
     *
     * @param path  the data path
     * @param value the data value
     */
    @Override
    public void set(final String path, final float value) {
        save(path, value);
    }

    /**
     * Save data
     *
     * @param path  the data path
     * @param value the data value
     */
    @Override
    public void set(final String path, final boolean value) {
        save(path, value);
    }

    /**
     * Save data
     *
     * @param path  the data path
     * @param value the data value
     */
    @Override
    public void set(final String path, final Serializable value) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream object = new ObjectOutputStream(out);
            object.writeObject(value);
            object.flush();

            save(path, Base64.getEncoder().encodeToString(out.toByteArray()));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Save data
     *
     * @param path   the data path
     * @param values the data value
     */
    @Override
    public void set(final String path, final Iterable<String> values) {
        save(path, collectIterable(values));
    }

    /**
     * Save data
     *
     * @param path    the data path
     * @param section the data value
     */
    @Override
    public void set(final String path, final YamlContent section) {
        for (String key : section.getKeys(true)) {
            String fullPath = path + "." + key;
            save(fullPath, section.get(key));
        }
    }

    /**
     * Get the yaml file raw data
     *
     * @return the yaml raw data
     */
    @Override
    public Map<String, Object> raw() {
        return new LinkedHashMap<>(data);
    }

    /**
     * Get the yaml handle
     *
     * @return the yaml handle
     */
    @Override
    public Path handle() {
        return file;
    }

    /**
     * Save the current yaml file handler
     * to the specified file
     *
     * @param path the yaml file path
     * @return the saved yaml file
     * @throws YamlSaveException if the file fails to save
     * @throws YamlDumpException if the file fails to dump
     */
    @Override
    public YamlContent saveTo(final Path path) throws YamlSaveException, YamlDumpException {
        SimpleYamlContent handler = new SimpleYamlContent(path, data, source);
        handler.save();

        return handler;
    }

    /**
     * Save the yaml file
     *
     * @throws YamlSaveException if the file fails to save
     * @throws YamlDumpException if the file fails to dump
     */
    @Override
    public void save() throws YamlSaveException, YamlDumpException {
        if (file == null)
            return;

        try {
            if (source != null) {
                String dump = source.getDumper().dump(this);
                Files.write(file, dump.getBytes(StandardCharsets.UTF_8));
                return;
            }

            DumperOptions options = new DumperOptions();
            options.setLineBreak(DumperOptions.LineBreak.getPlatformLineBreak());
            options.setPrettyFlow(true);
            options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
            options.setAllowUnicode(true);
            options.setProcessComments(true);
            options.setIndent(2);

            Yaml yaml = new Yaml(options);
            yaml.dump(data, Files.newBufferedWriter(file));
        } catch (IOException ex) {
            throw new YamlSaveException(ex);
        }
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     * @throws YamlDumpException if the file fails to dump
     */
    @Override
    public String toString() throws YamlDumpException {
        if (source != null)
            return source.getDumper().dump(this);

        DumperOptions options = new DumperOptions();
        options.setLineBreak(DumperOptions.LineBreak.getPlatformLineBreak());
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setAllowUnicode(true);
        options.setProcessComments(true);
        options.setIndent(2);

        Yaml yaml = new Yaml(options);
        return yaml.dump(data);
    }

    private static <T> String collectionToString(final Collection<T> collection) {
        StringBuilder builder = new StringBuilder();
        for (Object element : collection)
            builder.append(element)
                    .append('\n');

        return builder.substring(0, builder.length() - 1);
    }

    private static <T> List<T> collectIterable(final Iterable<T> iterable) {
        if (iterable instanceof List)
            return (List<T>) iterable;

        if (iterable instanceof Collection)
            return new ArrayList<>((Collection<? extends T>) iterable);

        List<T> newList = new ArrayList<>();
        iterable.forEach(newList::add);

        return newList;
    }
}
