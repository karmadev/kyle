package com.github.karmadeb.kyle.reader;

import com.github.karmadeb.kyle.YamlContent;
import com.github.karmadeb.kyle.exception.YamlParseException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Represents a yaml reader. The reader
 * can read an InputStream and schematize
 * it
 */
public final class YamlReader {

    private final byte[] raw;

    private final YamlDumper dumper;

    /**
     * Create the yaml reader
     *
     * @param stream the stream reader
     * @throws YamlParseException if the stream fails to read
     */
    public YamlReader(final InputStream stream) throws YamlParseException {
        this.raw = getStreamBytes(stream);
        this.dumper = new YamlDumper(this.raw);
    }

    /**
     * Get the content of the
     * stream
     *
     * @return the yaml content
     */
    public YamlContent getContent() {
        return YamlContent.parse(this.raw,
                new ByteArrayInputStream(this.raw));
    }

    /**
     * Get the yaml dumper for the
     * resource
     *
     * @return the yaml dumper
     */
    public YamlDumper getDumper() {
        return this.dumper;
    }

    private static byte[] getStreamBytes(final InputStream is) throws YamlParseException {
        try (InputStream stream = is;
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[2048];
            int read;

            while ((read = stream.read(buffer)) != -1)
                out.write(buffer, 0, read);

            return out.toByteArray();
        } catch (IOException ex) {
            throw new YamlParseException(ex);
        }
    }
}
