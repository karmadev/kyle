package com.github.karmadeb.kyle.reader;

import com.github.karmadeb.kyle.YamlContent;
import com.github.karmadeb.kyle.exception.YamlDumpException;
import com.github.karmadeb.kyle.util.EnumUtils;
import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.comments.CommentLine;
import org.yaml.snakeyaml.comments.CommentType;
import org.yaml.snakeyaml.nodes.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The yaml dumper identifies a
 * yaml node and dumps it into a
 * string
 */
@SuppressWarnings("unused")
public final class YamlDumper {

    private final byte[] raw;
    private final YamlContent self;

    /**
     * Create the yaml dumper
     *
     * @param raw the raw yaml content
     */
    public YamlDumper(final byte[] raw) {
        this.raw = raw;
        this.self = YamlContent.parse(this.raw);
    }

    /**
     * Get the yaml type of the provided
     * key
     *
     * @param key the key of the value
     * @return the value yaml type
     */
    public YamlType getYamlType(final String key) {
        if (!this.self.isSet(key))
            return YamlType.UNDEFINED;

        Object value = this.self.get(key);
        return resolveType(value);
    }

    /**
     * Validates a value element with the present
     * value in the yaml
     *
     * @param path the path to the current yaml content
     * @param value the value to validate
     * @return if the value type of this yaml matches the value type of
     * the provided value
     */
    public boolean validateType(final String path, Object value) {
        YamlType valueType = resolveType(value);
        YamlType pathType = this.getYamlType(path);

        if (value instanceof Enum<?>) {
            Enum<?> eConst = (Enum<?>) value;
            value = eConst.name();
            valueType = YamlType.STRING;
        }

        if (pathType.equals(YamlType.UNKNOWN) || pathType.equals(YamlType.UNDEFINED))
            return false;

        if (pathType.equals(YamlType.ENUM)) {
            if (!valueType.equals(YamlType.STRING))
                return false;

            String raw = (String) value;
            String enumValue = EnumUtils.handlePotentialEnumValue(raw);
            if (enumValue == null)
                return false;

            return EnumUtils.enumConstantHas(this.self.getString(path), enumValue);
        }

        return pathType.equals(valueType);
    }

    /**
     * Dump the yaml file
     *
     * @return the dumped string
     * @throws YamlDumpException if the reader fails to
     * read the yaml
     */
    public String dump() throws YamlDumpException {
        return this.dump(this.self);
    }

    /**
     * Dump the yaml contents
     *
     * @param content the content
     * @return the dumped string
     * @throws YamlDumpException if the reader fails to read
     * the yaml
     */
    public String dump(final YamlContent content) throws YamlDumpException {
        InputStream resource = new ByteArrayInputStream(this.raw);
        StringBuilder rawBuilder = new StringBuilder();

        try (InputStreamReader isr = new InputStreamReader(resource, StandardCharsets.UTF_8)) {
            MappingNode data = makeMappingNode(isr);
            writeBlockComments(0, data, rawBuilder);

            List<NodeTuple> tuples = data.getValue();
            for (NodeTuple tuple : tuples) {
                ScalarNode key = (ScalarNode) tuple.getKeyNode();

                writeBlockComments(0, key, rawBuilder);
                writeNode("", content, tuple, 0, rawBuilder, key);
            }
        } catch (IOException ex) {
            throw new YamlDumpException(ex);
        }

        return rawBuilder.toString();
    }

    private static YamlType resolveType(final Object value) {
        if (value == null)
            return YamlType.NULL;
        if (value instanceof Map)
            return YamlType.YAML;
        if (value instanceof Iterable || value.getClass().isArray())
            return YamlType.LIST;
        if (value instanceof Number)
            return YamlType.NUMBER;
        if (value instanceof Boolean)
            return YamlType.BOOLEAN;
        if (value instanceof CharSequence) {
            CharSequence sequence = (CharSequence) value;
            String raw = sequence.toString();

            if (raw.startsWith("!")) {
                if (raw.contains(":")) {
                    raw = raw.split(":")[0];
                }

                String className = raw.substring(1);
                try {
                    Class<?> clazz = Class.forName(className);
                    if (clazz.isEnum())
                        return YamlType.ENUM;
                } catch (ClassNotFoundException | NoClassDefFoundError ignored) {}
            }

            return YamlType.STRING;
        }

        return YamlType.UNKNOWN;
    }

    private static void writeNode(final String path, final YamlContent content, final NodeTuple tuple,
                                  final int indent, final StringBuilder rawBuilder, final ScalarNode key) {
        Node value = tuple.getValueNode();
        if (value instanceof MappingNode) {
            writeMappingNode(path, content, indent, rawBuilder, key, (MappingNode) value);
            return;
        }
        if (value instanceof SequenceNode) {
            writeSequenceNode(path, content, indent, rawBuilder, key, (SequenceNode) value);
            return;
        }

        writeScalarNode(path, content, indent, rawBuilder, key, value);
    }

    private static void writeScalarNode(String path, final YamlContent content, final int indent, final StringBuilder rawBuilder, final ScalarNode key, final Node value) {
        String spaces = makeSpaces(indent);
        ScalarNode scalarValue = (ScalarNode) value;

        if (path.isEmpty()) {
            path = key.getValue();
        } else {
            path = String.format("%s.%s", path, key.getValue());
        }

        String stringValue = String.valueOf(content.get(path));
        Object v = content.get(path);
        if (v == null) stringValue = scalarValue.getValue();

        stringValue = stringValue.replace("\n", "\\n");

        Tag tag = scalarValue.getTag();
        String t = tag.getClassName();
        if (t.equals("null") || t.equals("str")) {
            stringValue = EnumUtils.handlePotentialEnumValue(stringValue);
            if (stringValue == null) {
                stringValue = "null";
            } else {
                if (stringValue.contains("'")) {
                    stringValue = "\"" + stringValue + "\"";
                } else {
                    stringValue = "'" + stringValue + "'";
                }
            }
        }

        rawBuilder.append(spaces)
                .append(key.getValue())
                .append(": ")
                .append(stringValue);

        writeInlineComments(value, rawBuilder);
        rawBuilder.append("\n");
    }

    private static void writeSequenceNode(String path, final YamlContent content, final int indent, final StringBuilder rawBuilder, final ScalarNode key, final SequenceNode value) {
        if (path.isEmpty()) {
            path = key.getValue();
        } else {
            path = String.format("%s.%s", path, key.getValue());
        }

        mapStringSequence(path, indent, key, value,
                content, rawBuilder);
    }

    private static void writeMappingNode(String path, final YamlContent content, final int indent, final StringBuilder rawBuilder, final ScalarNode key, final MappingNode value) {
        String spaces = makeSpaces(indent);

        if (path.isEmpty()) {
            path = key.getValue();
        } else {
            path = String.format("%s.%s", path, key.getValue());
        }

        rawBuilder.append(spaces)
                .append(key.getValue())
                .append(":");
        writeInlineComments(key, rawBuilder);
        rawBuilder.append("\n");

        mapString(path, indent + 1, value, content, rawBuilder);
    }

    private static void writeBlockComments(final int indent, final Node data, final StringBuilder rawBuilder) {
        String spaces = makeSpaces((indent == 0 ? 0 : indent + 1));

        List<CommentLine> comments = data.getBlockComments();
        if (comments != null) {
            for (CommentLine comment : comments) {
                String value = comment.getValue();
                if (comment.getCommentType().equals(CommentType.BLANK_LINE)) {
                    rawBuilder.append("\n");
                } else {
                    rawBuilder.append(spaces).append("#").append(value).append("\n");
                }
            }
        }
    }

    private static void writeInlineComments(final Node node, final StringBuilder rawBuilder) {
        List<CommentLine> comments = node.getInLineComments();
        if (comments != null) {
            for (CommentLine comment : comments) {
                String value = comment.getValue();
                if (!value.replaceAll("\\s", "").isEmpty()) {
                    rawBuilder.append(" ").append("#").append(value);
                }
            }
        }
    }

    private static MappingNode makeMappingNode(InputStreamReader isr) {
        LoaderOptions options = new LoaderOptions();
        options.setProcessComments(true);

        Yaml yaml = new Yaml(options);
        return (MappingNode) yaml.compose(isr);
    }

    private static void mapString(final String path, final int indent, final MappingNode map, final YamlContent content,
                                           final StringBuilder rawBuilder) {
        for (NodeTuple tuple : map.getValue()) {
            ScalarNode key = (ScalarNode) tuple.getKeyNode();

            writeBlockComments(indent, key, rawBuilder);
            writeNode(path, content, tuple, indent + 1, rawBuilder, key);
        }
    }

    private static void mapStringSequence(final String path, final int indent, final ScalarNode key,
                                            final SequenceNode sequence, final YamlContent content, final StringBuilder rawBuilder) {
        String spaces = makeSpaces(indent);

        List<String> nodes = getNodesForSequence(path, sequence, content);
        if (nodes.isEmpty()) {
            rawBuilder.append(spaces).append(key.getValue()).append(": []");
            writeInlineComments(sequence, rawBuilder);
            rawBuilder.append('\n');
            return;
        }

        rawBuilder.append(spaces).append(key.getValue()).append(":\n");
        for (String value : nodes)
            rawBuilder.append(spaces).append("  - ").append(value).append("\n");
    }

    private static List<String> getNodesForSequence(final String path, final SequenceNode sequence, final YamlContent content) {
        List<String> nodes = new ArrayList<>();
        if (content == null || content.get(path) == null) {
            for (Node node : sequence.getValue()) {
                nodes.add(formatValue(((ScalarNode) node).getValue()));
            }
        } else {
            for (String value : content.getList(path)) {
                nodes.add(formatValue(value));
            }
        }

        return nodes;
    }

    private static String makeSpaces(final int tabSize) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < (tabSize * 2); i++)
            builder.append(' ');

        return builder.toString();
    }

    private static String formatValue(String value) {
        value = value.replace("\n", "\\n");
        return value.contains("'") ? "\"" + value + "\"" : "'" + value + "'";
    }
}