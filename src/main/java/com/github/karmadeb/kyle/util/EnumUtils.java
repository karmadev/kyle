package com.github.karmadeb.kyle.util;

/*
 * This file is part of kyle, licensed under the MIT License.
 *
 *  Copyright (c) hetol (KarmaDev) <karmaconfigs@gmail.com>
 *  Copyright (c) contributors
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * Enum utilities
 */
public final class EnumUtils {

    private EnumUtils() {
        throw new UnsupportedOperationException("Initialization of utility class: EnumUtils");
    }

    public static boolean enumConstantHas(final String raw, final String enumConstantName) {
        if (raw.startsWith("!")) {
            String enumClassName = raw.substring(1);
            if (enumClassName.contains(":")) {
                enumClassName = enumClassName.split(":")[0];
            }

            try {
                Class<?> clazz = Class.forName(enumClassName);
                if (clazz.isEnum()) {
                    Enum<?>[] constants = (Enum<?>[]) clazz.getEnumConstants();
                    if (constants != null) {
                        for (Enum<?> e : constants) {
                            if (e.name().equalsIgnoreCase(enumConstantName))
                                return true;
                        }
                    }
                }
            } catch (ClassNotFoundException | NoClassDefFoundError ignored) {}
        }

        return false;
    }

    public static String handlePotentialEnumValue(final String raw) {
        String result = tryGetEnumValue(raw);
        if (result == null) return raw;

        return result;
    }

    public static String tryGetEnumValue(final String raw) {
        if (raw.startsWith("!")) {
            String enumClassName = raw.substring(1);
            String matchingDefaultValue = null;
            if (enumClassName.contains(":")) {
                enumClassName = enumClassName.split(":")[0];
                matchingDefaultValue = raw.split(":")[1];
            }

            try {
                Class<?> clazz = Class.forName(enumClassName);
                if (clazz.isEnum()) {
                    Enum<?> defaultValue = getEnumFromClass(clazz, matchingDefaultValue);
                    return (defaultValue == null ? null : defaultValue.name());
                }
            } catch (ClassNotFoundException | NoClassDefFoundError ignored) {}
        }

        return null;
    }

    public static Enum<?> getEnumFromClass(final Class<?> clazz, final String matchingDefaultValue) {
        Enum<?>[] constants = (Enum<?>[]) clazz.getEnumConstants();
        Enum<?> defaultValue = (constants.length > 0 ? constants[0] : null);
        if (matchingDefaultValue != null) {
            for (Enum<?> e : constants) {
                if (e.name().equalsIgnoreCase(matchingDefaultValue)) {
                    defaultValue = e;
                    break;
                }
            }
        }
        return defaultValue;
    }
}
